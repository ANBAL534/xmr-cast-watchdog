import javax.swing.*;
import java.io.*;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        /*
        args[0] -> number of installed GPUs, 1 to infinity
        args[1] -> Cast-XMR executable Path
        args[2] -> -r if this option is given we will launch extra apps before the regular execution of the program
        args[x] -> programs to launch if -r was given
         */

        System.out.println("Cast-XMR Watchdog Wrapper & Remote Supervisor");
        System.out.println("Version 1.3");
        System.out.println();

        if((!args[1].contains(".exe") && !args[1].contains(".bat")) ||  args.length == 0){

            System.out.println("Incorrect arguments, see info:");
            System.out.println("java -jar Supervisor.jar [numberOfGPUs] [PathToMiner] ([-r] [Path] ...)");
            System.out.println("java -jar Supervisor.jar 3 \"C:\\cast-xmr-start.bat\"");
            System.out.println("java -jar Supervisor.jar 3 \"C:\\cast-xmr-start.bat\" -r \"C:\\OverdriveNtool-set.bat\" \"C:\\restart64.exe\"");
            System.out.println("Take into account that extra programs will be given only 30s of execution time before executing the next program or the miner.");
            sleep(3000);
            System.exit(1);

        }

        try{
            if(args[2].equals("-r")){

                boolean thatsIt = false;
                int index = 3;
                while (!thatsIt){

                    try {
                        System.out.println("Executing: " + args[index]);
                        Process extra = Runtime.getRuntime().exec(args[index]);
                        index++;
                        int repeats = 0;
                        while(extra.isAlive()){
                            sleep(1000);//Give enough time to finish execution
                            if(extra.isAlive() && repeats >= 30)
                                extra.destroy();
                            repeats++;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        thatsIt = true;
                    } catch (IOException ex) {
                        System.out.println("EXTRA EXCEPTION - " + ex.getMessage());
                    }

                }

            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("No extra programs to execute");
        }

        try {

            System.out.println("Starting miner...");
            System.out.println("");

            Process miner = Runtime.getRuntime().exec(args[1]);

            final PrintWriter writer = new PrintWriter(miner.getOutputStream(), true);
            final BufferedReader in = new BufferedReader(new InputStreamReader(miner.getInputStream()));
            final BufferedReader error = new BufferedReader(new InputStreamReader(miner.getErrorStream()));

            System.out.println("Launching Watchdog...");
            Thread readerInterpreter = new Watchdog(in, Integer.parseInt(args[0]));
            readerInterpreter.start();

            System.out.println("Starting server...");
            Thread server = new Server();
            server.start();

            String line = "";
            while (line != null){

                line = error.readLine();
                System.out.println("Received console error -> " + line);
                // TODO get a more specific trigger
                if(line.toLowerCase().contains("index")){
                    Process restart = Runtime.getRuntime().exec("shutdown -t 0 –r –f");
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Exiting...");

        System.exit(0);

    }
}
