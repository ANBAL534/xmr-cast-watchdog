
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Watchdog extends Thread {

    public static final boolean DEBUG_MODE = true;

    private BufferedReader in;
    private int installedGPUs;
    private int gpuUpdates[];
    public static volatile float gpuHashrate[];

    public Watchdog(BufferedReader in, int installedGPUs){

        this.in = in;
        this.installedGPUs = installedGPUs;

        gpuUpdates = new int[installedGPUs];
        for (int i = 0; i < gpuUpdates.length; i++) {
            gpuUpdates[i] = 0;
        }
        gpuHashrate = new float[installedGPUs];
        for (int i = 0; i < gpuHashrate.length; i++) {
            gpuHashrate[i] = 0.0f;
        }

    }

    @Override
    public void run(){

        Thread timeOutWatchThread = new TimeOutWatch();
        timeOutWatchThread.start();

        String line = "";
        Pattern pattern = Pattern.compile("\\[\\S+\\]\\sGPU\\d\\s\\|\\s\\d\\dºC\\s\\|\\sFan\\s((\\d\\d\\d\\d)|(\\d\\d\\d))\\sRPM\\s\\|\\s((\\d\\d\\d\\d)|(\\d\\d\\d))\\.\\d\\sH\\/s");
        Matcher matcher;

        try{
            String hashrate;
            String GPU;

            while ( line != null ) {

                line = in.readLine();

                ((TimeOutWatch)timeOutWatchThread).setReset(true);  // Each time we receive an update we reset the counter

                System.out.println(line);

                matcher = pattern.matcher(line);

                if(matcher.find()){

                    if(DEBUG_MODE)
                        System.out.println(" [DEBUG] " + "Regex match found.");

                    hashrate = line.replaceAll("\\[\\S+\\]\\sGPU\\d\\s\\|\\s\\d\\dºC\\s\\|\\sFan\\s((\\d\\d\\d\\d)|(\\d\\d\\d))\\sRPM\\s\\|\\s", "");
                    hashrate = hashrate.replaceAll("\\sH\\/s", "");
                    GPU = line.replaceAll("\\[\\S+\\]\\sGPU", "");
                    GPU = GPU.replaceAll("\\s\\|\\s\\d\\dºC\\s\\|\\sFan\\s((\\d\\d\\d\\d)|(\\d\\d\\d))\\sRPM\\s\\|\\s", "");
                    int gpuIndex = Integer.parseInt(GPU);

                    gpuUpdates[gpuIndex]++;
                    if(DEBUG_MODE)
                        System.out.println(" [DEBUG] " + "GPU" + gpuIndex + " updates set to: " + gpuUpdates[gpuIndex]);
                    if(gpuIndex+1 == installedGPUs){
                        if(DEBUG_MODE)
                            System.out.println(" [DEBUG] " + "GPU" + 0 + " updates reset (gpuIndex{" + gpuIndex + "}+1 == installedGPUs{" + installedGPUs + "})");
                        gpuUpdates[0] = 0;
                    }else{
                        if(DEBUG_MODE)
                            System.out.println(" [DEBUG] " + "GPU" + gpuIndex + " updates reset");
                        gpuUpdates[gpuIndex+1] = 0;
                    }
                    gpuHashrate[gpuIndex] = Float.parseFloat(hashrate);

                    //Check for frozen GPUs
                    for (int i = 0; i < gpuUpdates.length; i++) {

                        if(gpuUpdates[i] > 1+installedGPUs){

                            //A GPU frozen, restart Windows
                            System.out.println(" * GPU" + i + " FROZEN. REBOOTING WINDOWS * ");
                            Runtime.getRuntime().exec("shutdown –r");

                        }

                    }

                }

                line = "";

            }
        }catch(Exception ex){
            System.out.println("WATCHDOG EXCEPTION! - " + ex.getMessage());
        }

    }

}
