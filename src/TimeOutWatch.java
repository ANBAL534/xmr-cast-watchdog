import java.io.IOException;

public class TimeOutWatch extends Thread {

    private volatile boolean reset = false;

    @Override
    public void run(){

        while (true){

            try {
                sleep(15000);
            } catch (InterruptedException e) {

                if(interrupted() && reset){
                    reset = false;
                    continue;
                }

            }

            if(!reset){

                //A GPU frozen, restart Windows
                System.out.println(" * A GPU FROZEN. REBOOTING WINDOWS * ");
                try {
                    Runtime.getRuntime().exec("shutdown –r");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            reset = false;

        }

    }

    public void setReset(boolean reset) {
        this.reset = reset;
        this.interrupt();
    }
}
