import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    /*
    Listen as something connects

    Read a number, the gpu index we want to know the hashrate

    Return the hashrate as a float

    Repeat until client closes connection.
     */

    public static final int PORT = 18003;

    @Override
    public void run(){

        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            System.out.println("SERVER EXCEPTION (1) - " + e.getMessage());
        }

        while (!isInterrupted()){

            try{

                Socket clientSocket = serverSocket.accept();
                PrintWriter out =
                        new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));

                String textReceived = "";
                while ((textReceived = in.readLine()) != null){

                    //We should receive a number that is a GPU index
                    int gpuIndex = Integer.parseInt(textReceived);
                    //We send the hashrate back
                    out.println(Watchdog.gpuHashrate[gpuIndex]);

                }

                out.close();
                in.close();
                clientSocket.close();

            } catch (Exception e) {
                System.out.println("SERVER EXCEPTION (2) - " + e.getMessage());
            }

        }

        try {
            serverSocket.close();
        } catch (IOException e) {
            System.out.println("SERVER EXCEPTION (3) - " + e.getMessage());
        }

    }

}
