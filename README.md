**Obsolete as XMRCast no longer operates**

## XMR Cast Watchdog
A simple watchdog that launches XMRCast with given configuration and reboots the systems if a GPU card crash is detected (and doesn't freezes the entire system)
It also implements a plain text socket server to get the hashrate of the watched GPUs.

## Usage

`java -jar Supervisor.jar [numberOfGPUs] [PathToMiner] ([-r] [Path] ...)`
The extra paths after -r are programs that will be executed before the miner, such as your overclocking script, a fan control program, etc.
Those extra programs will be given 30s of execution time before killing them.